import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()

export class DispesasService {

    urlBaseConsulta = environment.url + 'consulta/';

    urlConsultaDispesas = this.urlBaseConsulta + 'consulta';
    urlConsultaPorId = this.urlBaseConsulta + 'consulta/';
    urlDeletaDispensa = this.urlBaseConsulta + 'consulta?id=';
    urlListaCategorias = this.urlBaseConsulta + 'consulta/categorias';
    urlBusca = this.urlBaseConsulta + 'consulta/busca?despesa=';

    constructor(private http: HttpClient) { }

    public Insert(dados): Observable<any> {
        return this.http.post<any>(this.urlConsultaDispesas, dados);
    }

    Update(dados): Observable<any> {
    return this.http.put<any>(this.urlConsultaDispesas, dados);
    }

    listaConsulta(): Observable<any> {
      return this.http.get<any>(this.urlConsultaDispesas);
    }

    listaCategorias(): Observable<any> {
        return this.http.get<any>(this.urlListaCategorias);
    }

    ConsultaPorId(id) {
      return this.http.get<any>(this.urlConsultaPorId, id);
    }

    Delete(id): Observable<any> {
        return this.http.delete(this.urlDeletaDispensa + id);
    }

    Busca(despesa, categoria, preco){
      return this.http.get<any>(this.urlBusca + despesa + '&categoria=' + categoria  + '&preco=' + preco);
    }
}