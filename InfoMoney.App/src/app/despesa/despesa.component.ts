import { DispesasService } from './../services/dispesas.service';
import { Component, OnInit, Input } from '@angular/core';
import Swal from 'sweetalert2';
import { Despesa } from 'app/Models/despesa';
import { empty } from 'rxjs/Observer';

declare var $: any;

@Component({
    selector: 'despesa-cmp',
    moduleId: module.id,
    templateUrl: 'despesa.component.html'
})

export class DespesaComponent implements OnInit{
  
    listaDispesas: any;
    listaCategorias: any;

    cadastro: any

    @Input() busca = {
      idCategoria: 0,
      descricao: '',
      preco: 0
    }

    @Input() despesa = {
      id: null,
      idCategoria: null,
      descricao: '',
      preco: ''
    }

    id: any;
    idCategoria: any;
    descricao: any;
    preco: any;

    constructor(public dispesasService: DispesasService){}

    ngOnInit(){

      this.carregaDespesas();
      this.carregaCategorias();

    }

    carregaDespesas(){
      this.dispesasService.listaConsulta().subscribe(
        data => {
          this.listaDispesas = data;
          console.log(data);
        }
      );
    }

    carregaCategorias() {
      this.dispesasService.listaCategorias().subscribe(
        data => {
          this.listaCategorias = data;
          console.log(data);
        }
      )

    }

  ChamaModal(i, value) {

    if (value === 0) {

      this.despesa = {
        id: null,
        idCategoria: 1,
        descricao: '',
        preco: ''
      }

      this.cadastro = true;

      this.abreModal();

    } else {

      this.despesa = {
        id: this.listaDispesas[i].id,
        idCategoria: this.listaDispesas[i].idCategoria,
        preco: this.listaDispesas[i].preco,
        descricao: this.listaDispesas[i].descricao,
      }
      console.log(this.despesa);

      this.cadastro = false;

      this.abreModal();
    }
  }

  fechaModal() {
    $('#myModal').modal('hide');
  };

  abreModal() {
    $('#myModal').modal({
      show: true,
      keyboard: false,
      backdrop: false
    });
  };

  Buscar() {

    var dados = this.busca;

    if(dados.descricao == ""){
      Swal.fire('Oops...', 'Informe uma despesa para consulta!', 'warning');
    } else {

      this.dispesasService.Busca(dados.descricao, dados.idCategoria, dados.preco).subscribe(data => {
        if (data) {
  
          this.listaDispesas = data;
  
        } else {
          this.fechaModal();
          Swal.fire('Oops...', 'Erro ao realizar busca!', 'error');
        }
  
      },
        error => {
          this.fechaModal();
          Swal.fire('Oops...', 'Erro ao realizar busca!', 'error');
        }
      );

    }

  }

  LimparFiltro(){
    var dados = this.busca;

    dados.descricao = "";
    dados.idCategoria = 0;
    dados.preco = 0;
    this.carregaDespesas();

  }

  Insert() {

    var dados = this.despesa;

    var despesa = new Despesa();
    despesa.descricao = dados.descricao;
    despesa.preco = dados.preco;
    despesa.idcategoria = dados.idCategoria;

    this.dispesasService.Insert(despesa)
      .subscribe(data => {
        if (data) {

          this.fechaModal();
          this.listaDispesas = data;

          Swal.fire(
            'success',
            'Cadastro realizado com sucesso!',
            'success'
          );
        } else {
          this.fechaModal();
          Swal.fire('Oops...', 'Erro ao realizar cadastro!', 'error');
        }

      },
        error => {
          this.fechaModal();
          Swal.fire('Oops...', 'Erro ao realizar cadastro!', 'error');
        }
      );
  }

  Update(id){
  }

  Delete(id) {
    console.log(id);

      Swal.fire({
        title: 'Deletar?',
        text: "Deseja realmente deletar este item!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, Deletar!'
      }).then((result) => {

        if (result.value) {
          this.dispesasService.Delete(id)
            .subscribe(data => {
              if (data) {
                console.log(data);
                this.listaDispesas = data;

                Swal.fire(
                  'success',
                  'Item deletado com sucesso!',
                  'success'
                );
              } else {
                Swal.fire('Oops...', 'Erro ao realizar o delete!', 'error');
              }

            },
              error => {
                Swal.fire('Oops...', 'Erro ao realizar o delete!', 'error');
              }
            );
        }
      });
    }
}
