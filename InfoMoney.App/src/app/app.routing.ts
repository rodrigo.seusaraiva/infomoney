import { Routes } from '@angular/router';

import { DespesaComponent } from './despesa/despesa.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'despesa',
        pathMatch: 'full',
    },
    {
        path: 'despesa',
        component: DespesaComponent
    }
]
