﻿using System;
using System.Collections.Generic;

namespace InfoMoney.Model
{
    public class Categoria
    {
        public Categoria(int id, string descricao)
        {
            this.Id = id;
            this.Descricao = descricao;
        }

        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}
