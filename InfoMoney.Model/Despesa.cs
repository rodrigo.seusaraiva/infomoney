﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfoMoney.Model
{
    public class Despesa
    {
        public Despesa(int Id, int IdCategoria, string Descricao, double Preco)
        {
            this.Id = Id;
            this.IdCategoria = IdCategoria;
            this.Descricao = Descricao;
            this.Preco = Preco;
        }

        public int Id { get; set; }
        public int IdCategoria { get; set; }
        public string  Descricao { get; set; }
        public double Preco { get; set; }
    }
}
