﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InfoMoney.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InfoMoney.Api.Controllers.Consulta
{
    [Route("api/consulta/[controller]")]
    [ApiController]
    public class ConsultaController : ControllerBase
    {

        public List<Despesa> Listao()
        {
            var listaDespesas = new List<Despesa>(); 
            listaDespesas.Add(new Despesa(1, 1, "Apto", 900));
            listaDespesas.Add(new Despesa(2, 3, "Camiseta", 80));
            listaDespesas.Add(new Despesa(3, 3, "Sapato", 150));
            listaDespesas.Add(new Despesa(4, 4, "Find na Praia", 100));
            listaDespesas.Add(new Despesa(5, 1, "Rancho", 650));
            listaDespesas.Add(new Despesa(6, 1, "Frutas", 44.50));

            return listaDespesas;
        }

        public List<Categoria> ListaCategoria()
        {
            var listaCategoria = new List<Categoria>();
            listaCategoria.Add(new Categoria(1, "Alimentação"));
            listaCategoria.Add(new Categoria(2, "Aluguel"));
            listaCategoria.Add(new Categoria(3, "Vestuário"));
            listaCategoria.Add(new Categoria(4, "Transporte"));

            return listaCategoria;
        }

        [HttpGet]
        public IEnumerable<Despesa> Get()
        {
            var listaDespesas = Listao();

            return listaDespesas;
        }

        [HttpGet]
        [Route("Categorias")]
        public IEnumerable<Categoria> GetCategorias()
        {
            var listaCategoria = ListaCategoria();

            return listaCategoria;
        }

        [HttpGet]
        [Route("Busca")]
        public IEnumerable<Despesa> busca(string despesa, int categoria, double preco)
        {
            var listaDespesas = Listao();

            var result = listaDespesas.Where(x => x.Descricao.Contains(despesa) || x.IdCategoria == categoria || x.Preco == preco);

            return result;
        }

        [HttpGet("{id}")]
        public IEnumerable<Despesa> Get(int id)
        {
            var listaDespesas = Listao();
            return listaDespesas.Where(x => x.Id == id);
        }

        [HttpPost]
        public IEnumerable<Despesa> Post(Despesa despesa)
        {
            var listaDespesas = Listao();
            var qtd = listaDespesas.Count();
            despesa.Id = (qtd + 1);
            listaDespesas.Add(despesa);

            return listaDespesas;
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        [HttpDelete]
        public IEnumerable<Despesa> Delete(int id)
        {
            var listaDespesas = Listao();
            listaDespesas.RemoveAll((x) => x.Id == id);

            return listaDespesas; 
        }
    }
}
